
# Carbon anime recommendation model  
![alt text](https://xp.io/storage/2JGtYmGd.png)
### it's my **5th Semester University Project** (Bachelor of computer science)

#### About Carbon  
Content-based filtering is one popular technique of recommendation or recommender systems. The content or attributes of the things you like are referred to as "content."

Here, the system uses your features and likes in order to recommend you with things that you might like. It uses the information provided by you over the internet and the ones they are able to gather and then they curate recommendations according to that.
The goal behind content-based filtering is to classify products with specific keywords, learn what the customer likes, look up those terms in the database, and then recommend similar things.

This type of recommender system is hugely dependent on the inputs provided by users, some common examples included Google, Wikipedia, etc. For example, when a user searches for a group of keywords, then Google displays all the items consisting of those keywords. The below video explains how a content-based recommender works.

## Goal
I made it a goal to build a system that did not take into account the popularity of an anime, but rather focused on innate features (e.g. plot) to determine similarity and make recommendations. Carbon is my attempt to achieve this goal.

## Third Party Websites for (Web scraping)
#### [ANIME-PLANET](https://www.anime-planet.com/)
### [Web Scraping Model link for Anime Planet](https://gitlab.com/Aakash-Yadav/model-for-recommendation/-/blob/master/Anime/Data_Cleaning/data_cleaning.ipynb)

### OutPut Json File 

#### [ALL Web Scraping Model OutPut Json Zip file  ](https://drive.google.com/file/d/11vr4TT6pyE6sP0WsXondNSpUa6srri9T/view?usp=sharing)

## Website / Working

#### Search Page
![alt text](https://xp.io/storage/2JJfbxdQ.png)

#### Recommendation Page
![alt text](https://xp.io/storage/2JJtINZP.png)

### Check-out Website and Enjoy (Recommendations)
#### [CARBON_WEB_APPLICATION --->](https://carbon.pythonanywhere.com/)

### Conclusion
Looking at the methods of content-based recommendation we understood that a computer uses many processes to make our lives easier, one of them is the recommendation process.

### Thanks and Enjoy.

