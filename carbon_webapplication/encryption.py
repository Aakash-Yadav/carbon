from cryptography.fernet import Fernet

# key = Fernet.generate_key()

# with open('enc.key','wb') as f:
#     f.write(key)
x= open('carbon_webapplication/enc.key','rb')
data = x.read()
x.close()

f = Fernet(data)

def convert_into_encrypt(n:str):
    n = n.encode()
    return f.encrypt(n)

def convert_to_decrypt(n):
    value = (f.decrypt(n))
    return ''.join(map(chr, value))
