
from flask import Blueprint,render_template,redirect,url_for
from flask_login import login_required,current_user,logout_user
from .main import anime_recomend
from random import randint
from .db_models import Images,Anime_Dec

def user_exist_or_not():
    try:
        current_user.username
        return 1;
    except:
        return 0;

views = Blueprint("views",__name__);

@views.route('/home')
@views.route('/')
def home_page():
    out_not_user_data = [];
    name_ = Images.query.filter_by(id=randint(100,10000)).first().name
    name_of_anime = anime_recomend(name_);
    print(name_)
    for i,j in name_of_anime.items():
        if(len(out_not_user_data)==50):
            break;
        DEc = Anime_Dec.query.filter_by(name=i).first();
        out_not_user_data.append({
            1:Images.query.filter_by(name=i).first().image_link,
            'name':i,
            'DEC': DEc.DEC if DEc else 'Not Found',
            'Sim': 99 if j>=100 else j,                
        });
    print(user_exist_or_not())
    return render_template("home.html",user=user_exist_or_not(),out_send=out_not_user_data,name=name_);


@views.route("/Dashboard")
@login_required
def dashboard():
    return f"{current_user.username} and {current_user.email}";

@views.route('/LogOut')
@login_required  
def logout():
    logout_user()
    return redirect(url_for('auth.Login'));
