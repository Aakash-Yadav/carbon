function sleep(ms) {
  return new Promise((resolver) => setTimeout(resolver, ms));
}

function Gen_random_numbers(n) {
  let value_1 = Math.floor(Math.random() * 99);
  let value_2 = Math.floor(Math.random() * 99);

  let K = ["-", "+", "*"][Math.floor(Math.random() * 2)];

  let out_value = `Solve (${value_1} ${K} ${value_2}) = ?`;

  sleep(n).then(() => {
    document.getElementsByClassName("Random")[0].placeholder = out_value;
  });
}

function check_for_value() {
  if (
    (user_out = document
      .getElementsByClassName("Random")[0]
      .placeholder.slice(5, -3))
  ) {
    let k = eval(
      document.getElementsByClassName("Random")[0].placeholder.slice(5, -3)
    );
    if (k == document.getElementsByClassName("Random")[0].value) {
      console.log("YESS");
    } else {
      document.getElementsByClassName("Random")[0].value = "";
      document.getElementsByClassName("Random")[0].placeholder = "";
      Gen_random_numbers(500);
    }
  } else {
    document.getElementById("password")[0].value = "";
    document.getElementById("conf_password")[0].value = "";
  }
}
Gen_random_numbers(3000);
