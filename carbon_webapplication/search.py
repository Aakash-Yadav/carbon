
from flask import Blueprint,render_template,request
from flask_login import login_required,current_user
# from Cython_model.main import anime_recomend
from .main import anime_recomend
from .db_models import Images,Anime_Dec

search = Blueprint('search',__name__);

@search.route('/search',methods=["GET","POST"])
@login_required
def recomend():
    if request.method == "POST":
        anime_name = request.form.get("somthing");
        anime_reco = anime_recomend(anime_name);
        # print(anime_reco)
        out_not_user_data = [];
        for i,j in anime_reco.items():
            DEc = Anime_Dec.query.filter_by(name=i).first();
            out_not_user_data.append({
            1:Images.query.filter_by(name=i).first().image_link,
            'name':i,
            'DEC': DEc.DEC if DEc else 'Not Found',
            'Sim': 99 if j>=100 else j,                
        });
        return render_template("recommend_page.html",out_send=out_not_user_data,name=anime_name);

    return render_template('search_base.html',name='')

@search.route('/search_by_name/<n>')
@login_required
def recommend_by_pre(n):
    anime_name=n;
    anime_reco = anime_recomend(anime_name);
    # print(anime_reco)
    # anime_reco = anime_recomend(anime_name);
        # print(anime_reco)
    out_not_user_data = [];
    for i,j in anime_reco.items():
            DEc = Anime_Dec.query.filter_by(name=i).first();
            out_not_user_data.append({
            1:Images.query.filter_by(name=i).first().image_link,
            'name':i,
            'DEC': DEc.DEC if DEc else 'Not Found',
            'Sim': 99 if j>=100 else j,                
        });
    return render_template("recommend_page.html",out_send=out_not_user_data,name=anime_name);