from cpython cimport array
from json import loads, dumps
from collections import ChainMap

cdef dict anime_tags_dict
f1 = open("/media/akash/BE38-1A0D/ANIME_JSON_DATA/anime_tag_index.json", 'r')
anime_tags_dict = loads(f1.read())
f1.close()

cdef dict anime_all_data
f2 = open("/media/akash/BE38-1A0D/ANIME_JSON_DATA/save_anime_basic_data.json", 'r')
anime_all_data = loads(f2.read())
f2.close()

cdef dict anime_tag_id
f3 = open(
    "/media/akash/BE38-1A0D/ANIME_JSON_DATA/anime_tags_values_of_anime_id.json", 'r')
anime_tag_id = loads(f3.read())
f3.close()

cdef dict anime_studio_index
f4 = open("/media/akash/BE38-1A0D/ANIME_JSON_DATA/anime_studio_name_and_anime_name_index.json", 'r')
anime_studio_index = loads(f4.read())
f4.close()

cdef binary_search(array.array arr, int x):
    cdef int l = 0, r, mid
    r = len(arr)-1
    while(l <= r):
        mid = (l+r)//2
        if(arr[mid] == x):
            return 1
        elif(x < arr[mid]):
            r = mid-1
        elif(x > arr[mid]):
            l = mid+1
    return 0


cdef convert_tags_to_int(list anime_tags, int size):
    cdef int x
    cdef array.array owner_anime = array.array('i', [x for x in range(size)])
    for x in range((size)):
        owner_anime[x] = anime_tags_dict[anime_tags[x]]
    return owner_anime

cdef get_ratio_of_two_anime_tags(array.array user_anime, array.array predict):
    cdef int i, add
    add = 0
    for i in range(len(predict)):
        if binary_search(user_anime, predict[i]):
            add += 1
        else:
            continue
    return (add, len(user_anime))

cdef get_the_anime_list_of_tags(str name_of_anime, int key=0):

    cdef float rating
    if anime_all_data[name_of_anime]["rating"]:
        rating = anime_all_data[name_of_anime]["rating"]
    else:
        rating = 0

    cdef list anime_tags_we_have = anime_all_data[name_of_anime]['Tags']

    cdef array.array owner_anime_tags = convert_tags_to_int(anime_tags_we_have, len(anime_tags_we_have))

    cdef int i, j

    cdef array.array anime_same_tags = array.array('i', [])
    cdef list owner_studio_other_anime = []

    try:
        anime_std_name = owner_studio_other_anime = anime_studio_index[
            anime_all_data[name_of_anime]["studio"]]
    except:
        anime_std_name = 0

    if key and anime_std_name:
        owner_studio_other_anime = anime_std_name

        for i in range(len(owner_studio_other_anime)):
            anime_same_tags.append(owner_studio_other_anime[i])
    else:
        for i in range(len(anime_tags_we_have)):
            for j in anime_tag_id[anime_tags_we_have[i]]:
                anime_same_tags.append(j)

    anime_same_tags = array.array('i', sorted(
        list(dict.fromkeys(anime_same_tags))))

    cdef list anime_name_list = list(anime_all_data)

    cdef str name

    cdef list outer_array = []

    for i in anime_same_tags:

        name = anime_name_list[i]
        if name == name_of_anime:
            continue
        value = anime_all_data[name]['Tags']

        if not value or not anime_all_data[name]['rating'] or not rating:
            continue
        else:
            array_tags_other_anime = convert_tags_to_int(value, len(value))
            one, two = get_ratio_of_two_anime_tags(
                owner_anime_tags, array_tags_other_anime)

            if rating and anime_all_data[name]['rating'] and round((one/two)*100) != 0:
                anime_rat = anime_all_data[name]['rating']

                if abs(anime_rat-rating) <= 1.1:
                    if key:
                        outer_array.append((i, round((one/two)*100)+10))
                    else:
                        outer_array.append((i, round((one/two)*100)))
    return outer_array


cdef get_the_anime_year(str name_of_anime, int second_recomend=0):

    cdef list other_anime_ratio = get_the_anime_list_of_tags(name_of_anime)

    cdef list same_studio_anime = get_the_anime_list_of_tags(name_of_anime, 1)

    cdef list out_array_for_same_std = []
    cdef list out_array_for_other_anime = []
    # non studio
    cdef list anime_name_list = list(anime_all_data)
    cdef int i, j, k
    for i, j in other_anime_ratio:
        try:
            one = anime_all_data[name_of_anime]["year"]
            two = anime_all_data[anime_name_list[i]]["year"]
            k = abs(one-two)
            if k <= 5:
                j += 6
            elif k <= 10 and k >= 5:
                j += 10

            if(second_recomend):
                if(j > 50):
                    out_array_for_other_anime.append((i, j))
            else:
                out_array_for_other_anime.append((i, j))
        except:
            if(second_recomend):
                if(j > 50):
                    out_array_for_other_anime.append((i, j))
            else:
                out_array_for_other_anime.append((i, j))

    for i, j in same_studio_anime:
        try:
            one = anime_all_data[name_of_anime]["year"]
            two = anime_all_data[anime_name_list[i]]["year"]
            k = abs(one-two)
            if k <= 5:
                j += 6
            elif k <= 10 and k >= 5:
                j += 10
            if(second_recomend):
                if(j > 60):
                    out_array_for_other_anime.append((i, j))
            else:
                out_array_for_other_anime.append((i, j))
        except:
            if(second_recomend):
                if(j > 60):
                    out_array_for_other_anime.append((i, j))
            else:
                out_array_for_other_anime.append((i, j))

    cdef array.array array_of_other = array.array('i', [n[1] for n in out_array_for_other_anime])
    # print(array_of_other)
    cdef list v = []
    for i, j in out_array_for_other_anime:
        if j > (max(array_of_other)//2):
            v.append((anime_name_list[i], j))
    return v


def anime_recomend(str name):

    cdef dict anime_data = dict(get_the_anime_year(name.lower()))

    anime_data = dict(sorted(anime_data.items(), key=lambda item: item[1]))

    cdef list names = list(anime_data)
    cdef int length = len(anime_data)
    cdef int mid = 0
    cdef int i, j
    cdef out_results = []

    if(length <= 50):
        if(length <= 10):
            out_results.append(anime_data)
            mid = 0
        else:
            mid = length-10

        for i in range(length):
            if(length<20):
                out_results.append(dict(get_the_anime_year(names[i],0)))    
            out_results.append(dict(get_the_anime_year(names[i], 1)))

        out_results = (ChainMap(*[div for div in out_results]))

        return dict(sorted(list(dict(out_results).items()), key=lambda sub: (-sub[1], sub[0])))
    else:
        return dict(sorted(list(anime_data.items()), key=lambda sub: (-sub[1], sub[0])))
