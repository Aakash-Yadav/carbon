from flask_wtf import FlaskForm 
from wtforms.validators import DataRequired,Length,ValidationError,Email,EqualTo
from wtforms import StringField,EmailField,SubmitField,PasswordField


class Register_form(FlaskForm):
    
    name = StringField(validators=[DataRequired(),Length(min=4,max=50)],render_kw={"placeholder":"Username"});
    
    email = EmailField(validators=[DataRequired(),Email()],render_kw={'placeholder':"Email address"});
    
    password = PasswordField(validators=[DataRequired(),Length(min=5,max=20),EqualTo('conf_password',message="Password Must Match")],render_kw=
                             {"placeholder":"Password"});
    
    conf_password = PasswordField(validators=[DataRequired(),Length(min=5,max=20)],render_kw={"placeholder":'confirm password'});
    
    submit = SubmitField("Register");
    

class Login_form(FlaskForm):
    
    username = StringField(validators=[DataRequired(),Length(min=4,max=50)]);
    # email = EmailField(validator=[DataRequired(),Email()],render_kw={'placeholder':"Email address"});
    password = PasswordField(validators=[DataRequired(),Length(min=5,max=20)]);
    submit = SubmitField("Login");
    