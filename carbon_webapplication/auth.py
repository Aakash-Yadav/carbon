from flask import Blueprint,render_template,url_for,redirect,flash,session,request
from .forms_models import Register_form,Login_form
from .db_models import add_user,search_data_in_db
from flask_login import login_user
  
auth = Blueprint('auth',__name__);

@auth.route('/login',methods=["GET","POST"])
def Login():
    Form = Login_form();
    if(Form.validate_on_submit()):
        session['Ename'] = Form.username.data;
        session["password"] = Form.password.data;
        user_exist = search_data_in_db(username=session["Ename"],password_=session["password"]);
        if(user_exist[1]):
            login_user(user_exist[0])
            return redirect(url_for("search.recomend"))
        elif (not user_exist[1]):
            flash(user_exist[0]);
            
    return render_template('login_page.html',xfor=Form);

@auth.route('/register',methods=["GET","POST"])
def Signup():
    Form = Register_form();
    
    if Form.validate_on_submit():
        session["username"] = Form.name.data;
        session["email"] = Form.email.data;
        password = Form.password.data;
        
        add_user_to_db = add_user(name=session["username"],email=session["email"],password=password);
        
        if(isinstance(add_user_to_db,int)):
            return redirect(url_for("auth.Login"));
        else:
            flash(add_user_to_db);
    return render_template('register_user.html',xfor=Form);
