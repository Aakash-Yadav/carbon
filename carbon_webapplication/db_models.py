from datetime import datetime
from flask_login import UserMixin
from werkzeug.security import generate_password_hash,check_password_hash
from . import db 
from .encryption import convert_into_encrypt

class User_database(db.Model,UserMixin):
    
    id = db.Column(db.Integer, primary_key=True);
    username = db.Column(db.String(50), unique=True, nullable=False);
    email = db.Column(db.String(200), unique=True, nullable=False)
    password =  db.Column(db.String(200), unique=True, nullable=False)
    date_time = db.Column(db.String(15),default="%s"%(datetime.now().date()));
    # anime_data = db.relationship("Anime_data",backref='user_database',uselist=0);

    def __init__(self,username_given,email_given,password_given) -> None:
        self.username = username_given
        self.email = convert_into_encrypt(email_given);
        self.password = generate_password_hash(password_given)

class Anime_data(db.Model):
    id = db.Column(db.Integer, primary_key=True);
    username_name = db.Column(db.String(150), unique=True, nullable=False);
    anime_list = db.Column(db.Text);
    
    def __init__(self,anime_lis,user_id):
        self.anime_list = convert_into_encrypt(anime_lis);
        self.username_name = user_id;

class Images(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name  = db.Column(db.String(500), nullable=False)
    image_link = db.Column(db.String(500), nullable=False)

class Anime_Dec(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name  = db.Column(db.String(500), nullable=False)
    DEC = db.Column(db.Text)
       

######################## Reg
def add_user(name,email,password):
    query_1 = User_database.query.filter_by(username=name).first();
    query_2 = User_database.query.filter_by(email=email).first();
    if(query_1):
        return ("Username already exists");
    elif(query_2):
        return ("Email already in use");
    else:
        new_user = User_database(name,email,password);
        new_user_anime_id = Anime_data(anime_lis="",user_id=name);
        # add_user_data = Anime_data(anime_lis='0',user_id=new_user);
        db.session.add(new_user);
        db.session.add(new_user_anime_id);
        # db.session.add(add_user_data);
        db.session.commit();
        return 1;

def search_data_in_db(username,password_):
    
    data = User_database.query.filter_by(username=username).first();
    if data:
        if check_password_hash(data.password,password_):
            return (data, 1)
        else:
            return ("Username exist but password is invalid",0)
    else:
        return ("Username doesn't exist",0)
